package com.example.davaleba19.Retrofit

import com.example.davaleba19.retrofitStuff.Api
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitService {

    fun service(): Api {
        return Retrofit.Builder()
            .baseUrl("https://identitytoolkit.googleapis.com")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(Api::class.java)

    }

}