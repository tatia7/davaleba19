package com.example.davaleba19.Retrofit

import com.example.davaleba19.Models.UserModel
import com.example.davaleba19.App
import com.example.davaleba19.models.SignInModel
import com.example.davaleba19.models.SignUpResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Query

interface Api {

    @POST("/v1/accounts:signUp")

    suspend fun signUp(
        @Body user: UserModel, @Query("key")
        apiKey: String = App.key
    ): Response<SignUpResponse>


    @POST("/v1/accounts:signInWithPassword")
    suspend fun signIn(
        @Body user: UserModel, @Query("key")
        apiKey: String = App.key
    ): Response<SignInModel>
}