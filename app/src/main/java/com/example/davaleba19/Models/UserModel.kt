package com.example.davaleba19.Models

data class UserModel
    (
    val email: String?,
    val password: String?,
    val returnSecureToken:Boolean = true
)
