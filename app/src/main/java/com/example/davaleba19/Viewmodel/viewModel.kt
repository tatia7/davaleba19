package com.example.davaleba19.Viewmodel

import android.util.Log.d
import androidx.lifecycle.ViewModel
import com.example.davaleba19.Retrofit.RetrofitService
import com.example.davaleba19.Models.UserModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class viewModel : ViewModel() {


    lateinit var userInfo: UserModel


    private suspend fun registration() {
        val result = RetrofitService.service().signUp(userInfo)
        d("message", result.body().toString())
    }

    private suspend fun LoginIn() {
        val result = RetrofitService.service().signIn(userInfo)
        d("message", result.body().toString())
    }


    fun Registration() {
        CoroutineScope(Dispatchers.IO).launch {
            registration()
        }
    }

    fun Loginin(){
        CoroutineScope(Dispatchers.IO).launch {
            LoginIn()
        }
    }


}