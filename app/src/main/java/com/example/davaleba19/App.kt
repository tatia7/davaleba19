package com.example.davaleba19

import android.app.Application
import android.content.Context

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        key = apiKey()
        context = applicationContext
    }

    companion object {

        lateinit var context: Context

        lateinit var key:String

        init {
            System.loadLibrary("native-lib")
        }
    }


    external fun apiKey(): String


}