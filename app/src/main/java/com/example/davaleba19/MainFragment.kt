package com.example.davaleba19

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import com.example.davaleba19.Viewmodel.viewModel
import com.example.davaleba19.databinding.FragmentMainBinding
import com.example.davaleba19.Models.UserModel

class MainFragment : Fragment() {

    private val viewModel: viewModel by viewModels()
    private lateinit var binding: FragmentMainBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMainBinding.inflate(inflater, container, false)

        init()

        return binding.root
    }
    private fun init() {

        binding.btnReg.setOnClickListener {
            viewModel.Registration()
            observes()
            Toast.makeText(requireActivity(), viewModel.userInfo.email, Toast.LENGTH_SHORT).show()

        }

        binding.btnLogin.setOnClickListener {
            viewModel.Loginin()
            observes()
        }

    }


    private fun observes() {

        viewModel.userInfo =
            UserModel(binding.Email.text.toString(), binding.Password.text.toString())

    }

}